<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use DB;

class IndexController extends Controller
{
    public function inicio() {
     
         
         if (Session::get('sessionid')!="")
            {
                if(Session::get('sessiontipo')=="1" ){

                    $count = DB::table('enfermero')
                                     ->count();
                    $users = DB::table('usuario')
                                     ->where('usuario.id_tipousuario','!=',1)
                                     ->where('usuario.id_tipousuario','!=',2)
                                     ->count();
                    $request = DB::table('citas as c')
                               ->join('solicitud_consultas as sc','sc.id_cita','=','c.id_cita')
                               ->where('sc.id_cita','!=','c.id_cita')
                               ->count();


         $citas = DB::table('citas as c')
                               ->join('solicitud_consultas as sc','sc.id_cita','=','c.id_cita')
                               ->join('pacientes as p','p.id_paciente','=','c.id_paciente')
                               ->join('usuario as u','u.id_usuario','=','p.id_usuario')
                               ->select('u.nombre as nombre')
                               ->where('sc.id_cita','!=','c.id_cita')
                               ->get();

                    // dd($request);

                    return view('admin.index',compact('count','users','request','citas'));
                }
                if(Session::get('sessiontipo')=="2" ){
                    return view('layouts.nurse');
                }
                if(Session::get('sessiontipo')=="3"){
                    return redirect()->route('empieza');
                    }
            }
            else{
                return redirect()->route('login');
            }
    }

    public function index(){
        return view('pricipal.index');
    }

    public function nosotros(){
        return view('pricipal.nosotros');
    }
    

     public function working(){
        return view('pricipal.trabaja-con-nosotros');
    }


    public function layouts(){
         $request = DB::table('citas as c')
                               ->join('solicitud_consultas as sc','sc.id_cita','=','c.id_cita')
                               ->where('sc.id_cita','!=','c.id_cita')
                               ->count();
         $citas = DB::table('citas as c')
                               ->join('solicitud_consultas as sc','sc.id_cita','=','c.id_cita')
                               ->join('pacientes as p','p.id_paciente','=','c.id_paciente')
                               ->join('usuario as u','u.id_usuario','=','p.id_usuario')
                               ->select('u.nombre as nombre')
                               ->where('sc.id_cita','!=','c.id_cita')
                               ->get();
                            // dd($citas);

        return view('layouts.admin',compact('request','citas'));
    }




}



