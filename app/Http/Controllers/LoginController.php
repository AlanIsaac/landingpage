<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;
use Session;
use Alert;


class LoginController extends Controller
{
    public function login() {
         
        if (Session::get('sessionid')!="")
            {
            	 return redirect()->route('inicio');
            }
            else{
                 return view('login.index');
            }
          
    }

          
   

	public function valida(Request $request) {

		$email= $request ->email;
		$password= $request ->contraseña;

		$consulta= UsersModel::withTrashed()
		->where('email','=',$email)
		->where('password','=',$password)
		->get();

		if (count($consulta)==0) {
			Session::flash('error', 'El usuario no existe o la contraseña no es correcta, por favor intentalo de nuevo');
			return redirect()->route('login');	
		}

		else{

          if($consulta[0]->deleted_at !="")
          {
            Session::flash('error','El usuario esta desactivado. consulte a su administrador');
            return redirect()->route('login');
          }

		else {

			session::put('sessionid', $consulta[0]->id_usuario);
			session::put('sessionnombre', $consulta[0]->nombre);
			session::put('sessiontipo', $consulta[0]->id_tipousuario);
			session::put('sessionemail', $consulta[0]->email);
			session::put('sessionfoto', $consulta[0]->foto);
			session::put('sessionaviso', $consulta[0]->fecha_aviso_privacidad);

			$sessionid = session::get('sessionid');
			$sessionnombre = session::get('sessionnombre');
			$sessiontipo = session::get('sessiontipo');
			$sessionemail = session::get('sessionemail');
			$sessionfoto = session::get('sessionfoto');
			$sessionaviso = session::get('sessionaviso');

			Session::flash('info','Bienvenido '.$sessionnombre);
			return redirect()->route('inicio');
						
						
			}

		}
	}

	public function cerrar(Request $request) 
	   	{
		
		session::forget('sessionid');
		session::forget('sessionnombre');
		session::forget('sessiontipo');

		Session::flash('error','Sesion cerrada correctamente');
			return redirect()->route('login');
				
		}	


}
