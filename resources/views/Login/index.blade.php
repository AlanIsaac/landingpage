<!-- <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <title>::LOGIN::</title>
<style>
html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: -webkit-box;
  display: flex;
  -ms-flex-align: center;
  -ms-flex-pack: center;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  padding-top: 40px;
  padding-bottom: 100px;
  background-color: #f5f5f5;
}

#messageerror{
  display:scroll;
        position:fixed;
        bottom:550px;
        right:400px;
}   


.form-signin {
  width: 100%;
  max-width: 350px;
  padding: 10px;
  margin: 0 auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

</style>

    Custom styles for this template 
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
  <div class="d-flex flex-column">
    <div class="d-flex justify-content-center">
      @if($message=Session::get('error'))
          <div class="alert alert-danger" role="alert">
            {{$message}}
          </div>
      @endif
    </div>
    <div class="d-flex justify-content-center">
      <form  class="form-signin" action="{{route('valida')}}" method="POST">
                {{ csrf_field() }}
                <h1 class="h3 mb-3 font-weight-normal">Porfavor inicie sesión</h1>
                <label for="inputEmail" class="">Correo electronico</label>
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="introduce tu correo" required autofocus>
                <label for="inputPassword" class="">Contraseña</label>
                <input type="password" id="inputPassword" class="form-control" name="contraseña" placeholder="Introduce tu contraseña" required>
                <div class="checkbox mb-3">
                        <label>
                        <input type="checkbox" value="remember-me"> Recordarme
                        </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sessión</button>
      </form>
    </div>
  </div>
</body>
</html> -->

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Inicio Sesión</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Slide Login Form template Responsive, Login form web template, Flat Pricing tables, Flat Drop downs Sign up Web Templates, Flat Web Templates, Login sign up Responsive web template, SmartPhone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

   <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

  <!-- Custom Theme files -->
  <link href="{{asset('login/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('login/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all" />
  <!-- //Custom Theme files -->
  <!-- web font -->
  <link href="//fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">
  <!-- //web font -->
 <style type="text/css">
   @import url('https://fonts.googleapis.com/css?family=Quicksand&display=swap');

.alert{
  width:50%;
  margin:20px auto;
  padding:30px;
  position:relative;
  border-radius:5px;
  box-shadow:0 0 15px 5px #ccc;
}
.close{
  position:absolute;
  width:30px;
  height:30px;
  opacity:0.5;
  border-width:1px;
  border-style:solid;
  border-radius:50%;
  right:15px;
  top:25px;
  text-align:center;
  font-size:1.6em;
  cursor:pointer;
}


 </style>
</head>
<body>



<!-- main -->
<div class="w3layouts-main"> 
  <div class="bg-layer">
    <h1>Inicia Sesión para Comenzar</h1>
     <div class="d-flex justify-content-center">
      @if($message=Session::get('error'))
          <div class="alert alert-danger" role="alert">
            <h6 style="color:white;">{{$message}}</h6>
          </div>
      @endif
    </div>
    <div class="header-main">
      <div class="main-icon">
        <span class="fa fa-eercast"></span>
      </div>
      <div class="header-left-bottom">
    
        <form  action="{{route('valida')}}" method="post">
         {{ csrf_field() }}
          <div class="icon1">
            <span class="fa fa-user"></span>
            <input type="email" placeholder="Dirección de Correo" name="email" required=""/>
          </div>
          <div class="icon1">
            <span class="fa fa-lock"></span>
            <input type="password" placeholder="Correo electronico" name="contraseña" required=""/>
          </div>
          <div class="login-check">
             <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Recordar</label>
          </div>
          <div class="bottom">
            <button class="btn">Iniciar Sesión</button>
          </div>
          <div class="links">
            <p><a href="#">¿Olvidaste Tu contraseña?</a></p>
            <p class="right"><a href="#">¿Nuevo Usuario? Registrate</a></p>
            <div class="clear"></div>
          </div>
        </form> 
      </div>
      <div class="social">
        <ul>
          <li>o inicia sesión con: </li>
          <li><a href="#" class="facebook"><span class="fa fa-facebook"></span></a></li>
          <li><a href="#" class="twitter"><span class="fa fa-twitter"></span></a></li>
          <li><a href="#" class="google"><span class="fa fa-google-plus"></span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>  
<!-- //main -->

</body>
</html>