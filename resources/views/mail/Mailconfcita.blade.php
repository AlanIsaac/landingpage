<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmación de la cita</title>
    <link rel="stylesheet" href="/static/css/bootstrap.css">
  <link rel="stylesheet" href="/static/css/bootstrap-theme.css">

</head>
<body>
<p align="center">¡Hola desde el equipo de APE - Agencia Profesional de Enfermería!</p><br><br><br>
<font face="Comic Sans MS,Arial,Verdana">Se detecto el registro de una cita con nosotros a nombre de su cuenta "{{Session::get('sessionnombre')}}"<br><br><br><br>

<table align="center">
<tr><td><strong>Paciente : </strong><td></td><td></td>{{$pac}}</td></tr>
<tr><td><strong>Fecha de la cita :  </strong><td></td><td></td>{{$fecha}}</td></tr>
<tr><td><strong>Hora de la cita :  </strong><td></td><td></td>{{$hour}}</td></tr>
<tr><td><strong>Municipio de la visita :  </strong><td></td><td></td>{{$municipio}}</td></tr>
<tr><td><strong>Colonia de la visita :  </strong><td></td><td></td>{{$colonia}}</td></tr>
<tr><td><strong>Calle de la visita :  </strong><td></td><td></td>{{$calle}}</td></tr>
<tr><td><strong>Celular de la visita :  </strong><td></td><td></td>{{$cell_seg}}</td></tr>
<tr><td><strong>Telefono de la visita :  </strong><td></td><td></td>{{$telefono_seg}}</td></tr>
</table>
    <br>
   <br>
    Si no fue usted, desea cancelar la cita o ver mas detalles de la misma  de click en el siguiente enlace.<br><br>
    <a href="{{URL::action('DatesController@citaspro')}}">
                        <button type="button" class="btn btn-gradient-warning btn-rounded btn-fw">Detalles-Cancelar</button>
                    </a>
</font><br>
<script src="/static/js/bootstrap.js"></script>
</body>
</html>