  <footer class="ftco-footer ftco-section img" style="background-image: url({{asset('template/images/footer-bg.jpg')}});">
    	<div class="overlay"></div>
      <div class="container-fluid px-md-5">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-3">
              <h2 class="ftco-heading-2">APE</h2>
              <h4 class="ftco-heading-2">¿Quieres saber mas acerca de nosotros?</h4>
              <ul class="list-unstyled">
               <li><a href="{{url('principal/nosotros')}}"><span class="icon-long-arrow-right mr-2"></span>Checa Nuestra Misión, Visión y Objetivos Aqui</a></li>
              </ul>

              <p>Siguenos en nuestras redes sociales.</p>
              <ul class="ftco-footer-social list-unstyled mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/Agencia-Profesional-de-Enfermer%C3%ADa-104327520974846/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-3 ml-md-3">
              <h2 class="ftco-heading-2">Cuidados</h2>
              <ul class="list-unstyled">
                <li><span class="icon-long-arrow-right mr-2"></span>Guardia de 8 horas</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Guardia de 12 horas</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Guardia de 24 horas</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Cuidados Paliativos 8 horas</li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-3 ml-md-3">
              <h2 class="ftco-heading-2">Servicios</h2>
              <ul class="list-unstyled">
                <li><span class="icon-long-arrow-right mr-2"></span>Toma de glucometria capilar</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Toma de tensión arterial</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Suministrar medicamentos</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Curación de heridas crónicas y agudas</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Venoclisis (sueros)</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Realización de vendajes</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Curación de ulseras por presión</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Inyecciones intramusculares</li>
                <li><span class="icon-long-arrow-right mr-2"></span>Mas servicios...</li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-3">
            	<h2 class="ftco-heading-2">¿Tienes Preguntas?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
	                <li><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></li>
	                <li><span class="icon icon-envelope pr-4"></span><span class="text">APEagenciadeenfermeria@gmail.com</span></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
	
            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> APE  <i class="icon-heart color-danger" aria-hidden="true"></i> Agencia Profesional de Enfermeria  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    