<div class="py-1 bg-black top">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+ 1235 2355 98</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">APEagenciadeenfermeria@gmail.com</span>
					    </div>
					    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end">
						    <p class="mb-0 register-link"><a href="{{route('login')}}" class="mr-3">Inicia Sesión</a><a href="#">Registrate</a></p>
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
	    <div class="container">
	    	<img src="{{asset('template/images/logofeo.jpg')}}" style="height: 80px; width: 60px;" class="navbar-brand">
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	          <li class="nav-item"><a href="#home-section" class="nav-link"><span>Inicio</span></a></li>
	          <li class="nav-item"><a href="#about-section" class="nav-link"><span>Nosotros</span></a></li>
	          <li class="nav-item"><a href="#department-section" class="nav-link"><span>Servicios</span></a></li>
	           <li class="nav-item"><a href="#testimonios" class="nav-link"><span>Testimonios</span></a></li>
	          <li class="nav-item"><a href="#fq" class="nav-link"><span>Preguntas Frecuentes</span></a></li>
	          <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contáctanos</span></a></li>
	          <li class="nav-item cta mr-md-2"><a href="{{route('login')}}" class="nav-link">Agenda Cita</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>