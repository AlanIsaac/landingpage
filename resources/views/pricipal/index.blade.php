@extends('pricipal.template')

@section ('content')

<style type="text/css">
	#maps {
        height: 300px;  /* The height is 400 pixels */
        width: 90%;  /* The width is the width of the web page */
       }


.c3-bg, .bg1, .button, .hero-slider .owl-dots .owl-dot.active, .single-service-2:hover .inner, .single-portfolio .inner .portfolio-img .hover-content, .faq .single-item.active, .single-faq h2 span, .footer_widget .nw_form button, .tabs-area .tabs-nav li a.active, .single-team .inner .team-img::after, .single-pricing.active .inner, .active.single-pricing-2 .inner, .single-pricing-2.active .inner .title .price {
    background-color: #4698ff;
}



	 *   07. faq-area
 ***************************** */
.faq-area .section-title {
  margin-bottom: 60px; }

.faq {
  padding: 0;
  margin: 0;
  list-style: none; }
  .faq .single-item {
    background-color: #fff;
    padding: 20px;
    padding-left: 50px;
    line-height: 28px;
    font-weight: 700;
    margin-bottom: 10px;
    cursor: pointer;
    border-radius: 4px;
    position: relative; }
    .faq .single-item::after {
      content: "+";
      width: 25px;
      height: 25px;
      border-radius: 50%;
      background-color: #222;
      color: #fff;
      position: absolute;
      left: 15px;
      top: 15px;
      text-align: center;
      line-height: 25px;
      font-size: 20px; }
    .faq .single-item.active::after {
      content: "-";
      background-color: #fff;
      color: #222; }
    .faq .single-item.active {
      color: #fff; }
      .faq .single-item.active h4 {
        color: #fff; }
    .faq .single-item h4 {
      font-size: 16px;
      margin: 0; }
    .faq .single-item .content {
      padding-top: 15px;
      font-size: 14px;
      font-weight: 400;
      display: none; }

.single-faq {
  border: 1px solid #cad4de;
  padding: 35px;
  margin-bottom: 30px;
  border-radius: 5px;
  color: #7d91aa;
  font-size: 14px;
  font-weight: 300; }
  .single-faq h2 {
    font-size: 24px;
    margin-bottom: 20px; }
    .single-faq h2 span {
      height: 40px;
      width: 40px;
      text-align: center;
      line-height: 40px;
      color: #fff;
      border-radius: 4px;
      margin-right: 20px; }
      @media only screen and (max-width: 991px) {
        .single-faq h2 span {
          font-size: 18px;
          line-height: 30px;
          height: 30px;
          width: 30px;
          margin-right: 15px; } }

.faq-sidebar-wrap {
  border: 1px solid #cad4de;
  margin-bottom: 30px;
  padding: 35px;
  border-radius: 4px; }
  @media only screen and (max-width: 991px) {
    .faq-sidebar-wrap {
      padding: 15px; } }
  .faq-sidebar-wrap h3 {
    font-size: 20px;
    font-weight: 400;
    margin: 0 0 15px; }

.faq-sidebar {
  padding: 0;
  list-style: none;
  margin: 0; }
  .faq-sidebar li {
    position: relative;
    padding-left: 20px; }
    .faq-sidebar li a:hover {
      color: #222; }
    .faq-sidebar li span {
      color: #7d91aa;
      position: absolute;
      left: 0;
      top: 0; }

/*****************************

	/* ==========================================================================================================
										BOTTOM
   ========================================================================================================== */
.fh5co-bottom-outer {
  width: 100%;
  height: 572px;
  background: url("{{asset('template/images/download-section-bg.jpg')}}") no-repeat center;
  margin-top: 120px;
  overflow-x: hidden;
}
.fh5co-bottom-outer .fh5co-bottom-inner {
  margin-top: 270px;
}
.fh5co-bottom-outer .fh5co-bottom-inner .col-sm-6 h1 {
  color: #fff;
  text-transform: uppercase;
  font-size: 30px;
  margin-bottom: 30px;
}
.fh5co-bottom-outer .fh5co-bottom-inner .col-sm-6 p {
  width: 350px;
  color: #fff;
  font-size: 14.5px;
  line-height: 1.85em;
}
.fh5co-bottom-outer .fh5co-bottom-inner .col-sm-6 img {
  margin-top: 25px;
}


/*****************************
 *   04. service-area
 ***************************** */
.single-service, .single-service-2, .single-service-3 {
  margin: 15px 0; }
  .single-service .inner, .single-service-2 .inner, .single-service-3 .inner {
    background-color: #fff;
    border-radius: 7px;
    padding: 30px; }
    .single-service .inner:hover, .single-service-2 .inner:hover, .single-service-3 .inner:hover {
      box-shadow: 2px 2px 25px rgba(0, 0, 0, 0.15); }
    .single-service .inner .title, .single-service-2 .inner .title, .single-service-3 .inner .title {
      overflow: hidden;
      margin-bottom: 15px; }
      .single-service .inner .title .icon, .single-service-2 .inner .title .icon, .single-service-3 .inner .title .icon {
        float: left;
        font-size: 26px; }
      .single-service .inner .title h4, .single-service-2 .inner .title h4, .single-service-3 .inner .title h4 {
        padding-left: 50px;
        font-size: 20px;
        margin: 3px 0 0; }
    .single-service .inner .content, .single-service-2 .inner .content, .single-service-3 .inner .content {
      font-weight: 300;
      font-size: 14px;
      color: #2d3666;
      line-height: 26px; }
      .single-service .inner .content p:last-child, .single-service-2 .inner .content p:last-child, .single-service-3 .inner .content p:last-child {
        margin-bottom: 0; }
  .single-service.bordered .inner, .bordered.single-service-2 .inner, .bordered.single-service-3 .inner {
    border: 1px solid #cad4de;
    box-shadow: none; }

.single-service-2 {
  text-align: center; }
  .single-service-2 .inner {
    border: 1px solid #cad4de;
    border-radius: 5px;
    padding: 85px 40px; }
    .single-service-2 .inner .content {
      font-size: 16px;
      color: #7288b5;
      margin-top: 25px; }
    .single-service-2 .inner .title {
      text-align: center; }
      .single-service-2 .inner .title .icon {
        float: none;
        display: block;
        font-size: 40px;
        margin-bottom: 30px; }
      .single-service-2 .inner .title h4 {
        padding-left: 0; }
  .single-service-2:hover .inner {
    color: #fff; }
    .single-service-2:hover .inner .content,
    .single-service-2:hover .inner .title h4 {
      color: #fff; }

.single-service-3 {
  margin: 30px 0; }
  .single-service-3 .inner {
    background: transparent;
    padding: 0;
    color: #fff; }
    .single-service-3 .inner:hover {
      box-shadow: none; }
    .single-service-3 .inner .content,
    .single-service-3 .inner .title h4 {
      color: #fff;
      padding-left: 100px; }
    .single-service-3 .inner .title h4 {
      font-size: 24px; }
    .single-service-3 .inner .content {
      font-size: 16px; }
    .single-service-3 .inner .title .icon {
      background-color: rgba(255, 255, 255, 0.2);
      width: 70px;
      height: 70px;
      text-align: center;
      line-height: 70px;
      border-radius: 4px;
      position: absolute;
      left: 15px;
      top: 0;
      font-size: 34px; }
    .single-service-3 .inner:hover .title .icon {
      background-color: #fff; }

/*****************************
</style>

<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('template/images/seguro1.jpg')}}'); width: 100%; height: 50%;" data-section="home" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-6 pt-5 ftco-animate">
          	<div class="mt-5">
	            <h1 class="mb-4">Bienvenido a A.P.E<br></h1>
	            <p class="mb-4">Estamos aqui para brindarte servicios de calidad por medio de profesionales altamente capacitados.Con el objetivo de procurar el bienestar de  nuestros empleados y usuarios.</p>
	            <p><a href="{{route('login')}}" class="btn btn-primary py-3 px-4">Contratar Servicio</a></p>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center">
    					<img src="{{asset('template/images/logofeo.jpg')}}" style="width: 500px; height: auto;">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 pl-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">

                         <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4" align="center"><span>¿Qué hacemos?</span></h2>
			            <p align="justify">A.P.E es una agencia integrada por profesionales de la salud basada en metodología científica aplicada a las necesidades de la población, para el cuidado específico y paliativo de pacientes, así como, brindar capacitación a personal de enfermería y a aquellas dedicadas al cuidado asistencial a domicilio.</p>
			          </div>

                          



	    			    <!-- <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4" align="center"><span>¿Quiénes Somos?</span></h2>
			            <p align="justify">A.P.E es una agencia integrada por profesionales de la salud basada en metodología científica aplicada a las necesidades de la población, para el cuidado específico y paliativo de pacientes, así como, brindar capacitación a personal de enfermería y a aquellas dedicadas al cuidado asistencial a domicilio.</p>
			            <p><a href="#" class="btn btn-primary py-3 px-4">Contratar Servicio</a> <a href="#" class="btn btn-secondary py-3 px-4">Contáctanos</a></p>
			          </div> -->
			          <!-- <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4" align="center"><span>Misión</span></h2>
			            <p align="justify">Somos una agencia integrada por profesionales de la salud basada en metodología científica aplicada a las necesidades de la población, para el cuidado específico y paliativo de pacientes, así como, brindar capacitación a personal de enfermería y a aquellas dedicadas al cuidado asistencial a domicilio.</p>
			            <br>
			            <h2 class="mb-2" align="center"><span>Visión</span></h2>
			            <p align="justify">Ser una empresa líder y reconocida en el área de la salud, operando en el área de Lerma y sus alrededores, integrada por personal de enfermería altamente capacitado y con un enfoque en la calidad de los servicios y altos estándares de calidad..</p>
			            <p><a href="#contact-section" class="btn btn-secondary py-3 px-4">Contáctanos</a></p>
			          </div> -->
			        </div>
		        </div>
	        </div>
        </div>
     </div>
    </section>

   <section>
   	 
   	     <div class="service-area spt">
    <div class="container">
        <div class="section-title" data-margin="0 0 40px">
            <h2 align="center">¿Porqué Nosotros?</h2>
        </div>
        <div class="row">
            <div class="col-md-4 single-service-2">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                            <i class="fa fa-film"></i>
                        </div>
                        <h4>¿Por que elegirnos o por que usar nuestros servicios?</h4>
                    </div>
                    <div class="content">
                        <p>Somos la unica agencia en el mercado actual que ofrece servicio de atención y cuidados de enfermería mediante plataformas digitales, con el menor tiempo de respuesta, con los precios más competitivos, llevando hasta la comodidad de tu hogar el servicio de enfermería que solicites de manera práctica y segura. 
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 single-service-2">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                            <i class="fa fa-camera"></i>
                        </div>
                        <h4> ¿Por que usar nuestra aplicacion y nuestra Plataforma?</h4>
                    </div>
                    <div class="content">
                        <p>Es fácil de usar desde cualquier dispositivo movil o escritorio que tengas al alcance de tus manos, formarás parte de una nueva tendencia de consumo, al solicitar un servicio de enfermería no solo cuidas la salud, también cuidas de tu dinero y tú tiempo. </p>
                        <br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-4 single-service-2">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                            <i class="fa fa-music"></i>
                        </div>
                        <h4>¿Que nos diferencia de los demas?</h4>
                    </div>
                    <div class="content">
                        <p>La calidad, confianza y atención  que nuestros enfermeros ofrecen a los pacientes, la plataforma digital disponible para contactarnos las 24 horas del día, los 365 días del año, la gama de servicios con los precios justos que te mereces, nosotros ponemos al paciente primero.</p>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


   </section>

<section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 bg-light" id="department-section">
			<div class="container">
        <div class="row d-flex">
	        <div class="col-md-7 py-5">
	        	<div class="py-lg-5">
		        	<div class="row justify-content-center pb-5">
			          <div class="col-md-12 heading-section ftco-animate">
			            <h2 class="mb-3">Nuestros Servicios</h2>
			          </div>
			        </div>
			        <div class="row">
			        	<div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-ambulance"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">TENSIÓN ARTERIAL</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-doctor"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">ATENCIÓN INTEGRAL</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-stethoscope"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">CURACION DE HERIDAS</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-24-hours"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">VENDAJES</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-doctor"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">INYECCIONES INTRAMUSCULARES</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-stethoscope"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">DIALISÍS PERITONEAL</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-24-hours"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">CAMBIO DE BOLSA DE COLOSTOMIA</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-ambulance"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">Y MAS SERVICIOS...</h3>
			                <p></p>
			              </div>
			            </div>      
			          </div>
			        </div>
			      </div>
		      </div>
		      <div class="col-md-5 d-flex">
	        	<div class="appointment-wrap bg-light p-4 p-md-5 d-flex align-items-center">
		        	<form action="#" class="appointment-form ftco-animate">
						<h3>Registro gratis</h3>
		    				<div class="">
			    				<div class="form-group">
								<input type="text" class="form-control" placeholder="Nombre">
			    				</div>
		    				<div class="">
			    				<div class="form-group">
			            		<input type="text" class="form-control" placeholder="Email">
			    				</div>
			    				<div class="form-group">
			            		<input type="text" class="form-control" placeholder="Contraseña">
			    				</div>
		    				</div>
		    				<div class="">
			            <div class="form-group">
						<input type="submit" value="Registrarse" class="btn btn-secondary py-3 px-4">
			            </div>
		    				</div>
		    			</form>
		    		</div>
	        </div>
		    </div>
			</div>
		</section>
    <section>
		

     <div id="fh5co-download" class="fh5co-bottom-outer">
		<div class="overlay">
			<div class="container fh5co-bottom-inner">
				<div class="row">
					<div class="col-sm-6">
						<h1>¡¡También contámos con una APP!!</h1>
						<p>Ya esta disponible la aplicación para dispositivos moviles APE. Conéctate de la forma mas cómoda y secilla a la App, agenda desde ahi tu cita, paga y adquiere los servicios que necesitas sin la necesidad de salir de tu hogar.</p>
						<a class="wow fadeIn animated" data-wow-delay="0.25s" href="#"><img class="app-store-btn" src="{{asset('template/images/app-store-icon.png')}}" alt="App Store Icon"></a>
						<a class="wow fadeIn animated" data-wow-delay="0.67s" href="#"><img class="google-play-btn" src="{{asset('template/images/google-play-icon.png')}}" alt="Google Play Icon"></a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<section >
	 <div class="service-area bg2 sp">
    <div class="container">
        <div class="section-title">
            <h2 align="center">Acerca de Nuestro Personal de Trabajo</h2>
            <p>Validamos referencias y conocimientos, capacitamos constantemente y entrevistamos en persona para garantizar tu seguridad y la de tu familia.Estos son los requerimientos solicitados con los que cuenta cada uno de nuestros enfermeros dedicados al cuidado de la salud.</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 single-service">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                           <img src="{{asset('template/images/examen0.jpg')}}" style="width: auto; height: 200px;">
                        </div>
                        <h4>Certificaciones</h4>
                    </div>
                    <div class="content">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 single-service">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                           <img src="{{asset('template/images/examen2.jpg')}}" style="width: auto; height: 200px;">
                        </div>
                        <h4> Psicometricos</h4>
                    </div>
                    <div class="content">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 single-service">
                <div class="inner">
                    <div class="title">
                        <div class="icon">
                            <img src="{{asset('template/images/experiencia.jpg')}}"  style="width: auto; height: 180px;">
                        </div>
                        <br><br><br><br><br><br><br>
                        <h4 align="left">Experiencia Laboral</h4>
                    </div>
                    <div class="content">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

      <section>

     
			
	  </section>



   <!--  <section class="ftco-intro img" style="background-image: url({{asset('template/images/dr.jpg')}});">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
					<h2>Tu salud es nuestra prioridad</h2>
					<p>Podemos manegar cualquier situación médica que requieras</p>
						<p class="mb-0"><a href="#contact-section" class="btn btn-white px-4 py-3">Nuestra Ubicación</a></p>
					</div>
				</div>
			</div>
		</section> -->
		

		<!--Testimonios-->
    <section class="ftco-section testimony-section img" style="background-image: url({{asset('template/images/bg_3.jpg')}});" id="testimonios">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-4">Algunos Testimonios de Nuestros Clientes</h2>
          </div>
        </div>
              <div class="item">
                <div class="testimony-wrap text-center">
                  <div class="user-img" style="background-image: url({{asset('template/images/person_1.jpg')}})">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">Exelente Servicio. 🙋💕 <br> ¡denle like! ☺👍</p>
                    <p class="name">Eliza Salazar</p>
                    <span class="position">3 de octubre a las 23:42 </span>
                  </div>
                </div>
              </div>
      </div>
    </section>
    <!--Final Testimonios-->

    <section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 " id="fq">
    	<div class="faq-area sp bg2">
    <div class="container">
        <div class="section-title">
            <h2 align="center">Preguntas Frecuentes</h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="faq">
                    <div class="single-item">
                        <h4>¿Puedo tener varias personas registradas a mi cuenta?</h4>
                        <div class="content">
                      Si al crear una cuenta eres capaz de registrar varios pacientes que necesiten de nuestros cuidados a tu nombre.
                        </div>
                    </div>
                    <div class="single-item">
                        <h4>¿Que pasa si no pago el servicio?</h4>
                        <div class="content">
                          Desde el momento en el que no pagas, tu cuenta queda retenida y no podras solicitar mas servicios hasta que allas pagado.
                        </div>
                    </div>
                    <div class="single-item">
                        <h4>¿Puedo cancelar la solicitud de servicio?</h4>
                        <div class="content">
                        Por supuesto una ves agendada la cita, tu podras cancelar la cita mientras y cuando lo hagas con anticipación, si tu deseas cancelar la cita cuando el enfermero esta en camino o ya se encuentra en tu 
                        domicilio  el cobro se hara de todas maneras.
                        </div>
                    </div>
                    <div class="single-item">
                        <h4>¿Como puedo pagar el servicio?</h4>
                        <div class="content">
                        Contamos con distintos metodos de pago, puedes  pagar el o los servicios con su cuenta PayPal, Tarjetas de Cedito o Debito y pagar en efectivo.                   
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-img text-center">
                    <img src="{{asset('template/images/preguntas.jpg')}}" alt="faq" style="width: auto; height: 200px;">
                </div>
            </div>
        </div>
    </div>
</div>
    </section>



 <section>
 	

 <section class="ftco-section testimony-section img" style="background-image: url({{asset('template/images/fondoguapo.jpg')}});" id="testimonios">
    <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-left pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-4">¿No sabes como agendar tu cita?</h2>
          </div>
        </div>
              <div class="item">
                <div class="testimony-wrap justify-content-center">
                  <div class="text px-4">
                  	<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                       <h4 class="mb-4" style="color: white;">Checa este VideoTutorial donde te explicamos lo facil y sencillo que es contratar nuestros servicios</h4>
                   </div>
                  </div>
                </div>
              </div>
                <div class="col-lg-6 justify-content-right">
                                <div class="inner embed-responsive embed-responsive-16by9" data-margin="25px 0 0">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/GYtUAYYDPo0?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                 </div>
      </div>
    </section>




 </section>
    <!--Inicio Contactano-->

    <section class="ftco-section contact-section" id="contact-section">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Contáctanos</h2>
            <p>Aqui algunos de las ópciones por las cúales podrias contáctarnos </p>
          </div>
        </div>
        <div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<h3 class="mb-4">Dirección</h3>
	            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<h3 class="mb-4">Número de Contactó</h3>
	            <p><a href="tel://1234567920">+ 1235 2355 98</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<h3 class="mb-4">Correo de Contacto</h3>
	            <p><a href="#"></a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-user"></span>
          		</div>
          		<h3 class="mb-4">Trabaja con Nosotros</h3>
	            <p>¿Quieres formar parte de nuestro equipo de trabajo?</p>
	            	<a href=""><input type="submit" value="Enviar CV" class="btn btn-primary py-3 px-4"></a>
	        </div>
          </div>
        </div>
        <div class="row no-gutters block-9">
          <div class="col-md-6 order-md-last d-flex">
            <form action="{{route('mailinfo')}}" method="get">
            	<span>Dudas y/o sugerencias</span>
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Tu Nombre">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Tu Correo">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="asunto" placeholder="Asuntó">
              </div>
              <div class="form-group">
                <textarea name="mensaje" cols="30" rows="7"  class="form-control" placeholder="Mensaje"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Enviar Mensaje" class="btn btn-secondary py-3 px-5">
              </div>
            </form>
          
          </div>

          <div class="col-md-6 d-flex">
          	<div id="maps" class="bg-white">
          		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7532.991883333039!2d-99.5987506!3d19.2607875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd8bae683c2073%3A0x94983f4fa1cfaa56!2zTWV0ZXBlYywgTcOpeC4!5e0!3m2!1ses-419!2smx!4v1570675703596!5m2!1ses-419!2smx" width="555" height="588" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          	</div>
          </div>
        </div>
      </div>
    </section>


@endsection