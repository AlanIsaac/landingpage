@extends('pricipal.template')

@section ('content')

<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('template/images/seguro1.jpg')}}'); width: 100%; height: 50%;" data-section="home" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-6 pt-5 ftco-animate">
          	<div class="mt-5">
	            <h1 class="mb-4">Bienvenido a A.P.E<br></h1>
	            <p class="mb-4">Estamos aqui para brindarte servicios de calidad por medio de profesionales altamente capacitados.Con el objetivo de procurar el bienestar de  nuestros empleados y usuarios.</p>
	            <p><a href="{{route('login')}}" class="btn btn-primary py-3 px-4">Contratar Servicio</a></p>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center">
    					<img src="{{asset('template/images/logofeo.jpg')}}" style="width: 500px; height: auto;">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 pl-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">

                 <!--   <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4" align="center"><span>¿Qué hacemos?</span></h2>
			            <p align="justify">A.P.E es una agencia integrada por profesionales de la salud basada en metodología científica aplicada a las necesidades de la población, para el cuidado específico y paliativo de pacientes, así como, brindar capacitación a personal de enfermería y a aquellas dedicadas al cuidado asistencial a domicilio.</p>
			          </div>
 -->
      
			          <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4" align="center"><span>Misión</span></h2>
			            <p align="justify">Somos una agencia integrada por profesionales de la salud basada en metodología científica aplicada a las necesidades de la población, para el cuidado específico y paliativo de pacientes, así como, brindar capacitación a personal de enfermería y a aquellas dedicadas al cuidado asistencial a domicilio.</p>
			            <br>
			            <h2 class="mb-2" align="center"><span>Visión</span></h2>
			            <p align="justify">Ser una empresa líder y reconocida en el área de la salud, operando en el área de Lerma y sus alrededores, integrada por personal de enfermería altamente capacitado y con un enfoque en la calidad de los servicios y altos estándares de calidad..</p>
			            <p><a href="#contact-section" class="btn btn-secondary py-3 px-4">Contáctanos</a></p>
			          </div>
			        </div>
		        </div>
	        </div>
        </div>
     </div>
    </section>

   

@endsection