<!DOCTYPE html>
<html lang="en">
  <head>
    <title>APE | Agencia Profesional de Enfermeria</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('template/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('template/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/ionicons.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('template/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">
  </head>
 <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
   

   @include('partials.nav')

 	 @yield('content')

   @include('partials.footer')
 

  <script src="{{asset('template/js/jquery.min.js')}}"></script>
  <script src="{{asset('template/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('template/js/popper.min.js')}}"></script>
  <script src="{{asset('template/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('template/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('template/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('template/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('template/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('template/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('template/js/aos.js')}}"></script>
  <script src="{{asset('template/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('template/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('template/js/google-map.js')}}"></script>
  <script src="{{asset('template/js/active.js')}}"></script>
  
  <script src="{{asset('template/js/main.js')}}"></script>
    
  </body>
</html>