<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Rutas de Login*/
Route::get('/acceso','LoginController@login')->name('login');
Route::POST('/valuelogin','LoginController@valida')->name('valida');
Route::get('/cerrar','LoginController@cerrar')->name('cerrar');

/*Index*/
Route::get('/inicio', 'IndexController@inicio')->name('inicio');
Route::get('/iniciousuario', 'UsersController@empieza')->name('empieza');

Route::get('',function(){
	return redirect()->route('index');
});

Route::get('principal/index', 'IndexController@index')->name('index');
Route::get('principal/nosotros', 'IndexController@nosotros')->name('nosotros');
Route::get('principal/trabaja-con-nosotros', 'IndexController@working');
///////////////////////CORREO ELECTRONICO///////////////////////////////
Route::get('mailinfo','MailController@store')->name('mailinfo');
///////////////////////END RUTAS TEMPLATE MIKEL///////////////////////////////
